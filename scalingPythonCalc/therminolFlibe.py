# here i will have two (or more) classes, one workspace class to keep reloading the other classes
# and one class to calculate all the scaling parameters

class scalingClass:


    def __init__(self):

        import numpy as np
        self.np = np

        self.initialiseScalingDefaults()

        self.therminolPropertiesObj = self.getTherminolPropertiesObj()
        self.flibePropertiesObj = self.getFlibePropertiesObj()

        self.cietObj = self.getCietObj()
        self.reactorObj = self.getReactorObj()

        # next i set the flibe and therminol temperatures to reactor inlet, 80C and 600C by default

        self.therminolPropertiesObj.setTemperature(80)
        self.flibePropertiesObj.setTemperature(600)

        self.setTherminolVelocity(0.1)
        self.setFlibeVelocity(0.6)

        self.setTherminolDiameter(1)
        self.setFlibeDiameter(1)

    def initialiseScalingDefaults(self):

        self.setFlowSimiltude(flowSimiltude=True)
        self.setPrandtlSimiltude(prandtlSimiltude=True)
        self.printStatus()



    def iterationOneScaleUp(self,cietPowerWatts = 8000, cietMassFlowrate = 0.18):

        self.scaleUpTest1(cietPowerWatts = cietPowerWatts, cietMassFlowrate = cietMassFlowrate)





    def scaleUpTest1(self,cietPowerWatts = 8000, cietMassFlowrate = 0.18):

        # first i'll set the temperatures of flibe and therminol

        # in fact therminol will be at 80C and 110C cold and hot temperatures respectively,

        # so i'll do some of the scaling twice

        # note: scaling does not account for natural convection nor radiation distortions

        self.therminolPropertiesObj.setTemperature(80)
        self.flibePropertiesObj.setTemperature(600)

        # i will then obtain Reynold's number of therminol, at mass flowrate of 0.18kg/s

        # this is first done by syncing things up with the cietObj
        
        self.setCietPowerWatts(8000)
        self.setCietMassFlowrate(0.18) # this is in kg/s 

        self.syncCietObj()

        # now we should be able to get a Reynold's number

        cietReynolds = self.getTherminolReynolds()

        print("therminol reynolds is " +str(self.therminolPropertiesObj.getTemperature())+"C :")
        print(cietReynolds)

        self.therminolPropertiesObj.setTemperature(90)
        self.syncCietObj()
        
        print("therminol reynolds is " +str(self.therminolPropertiesObj.getTemperature())+"C :")
        cietReynolds = self.getTherminolReynolds()
        print(cietReynolds)

        # now that we have reynolds number, we can then extract uD from such a reynolds number

        # we use the formula (Re)Therminol = (Re)Flibe
        # (Re)Therminol = (uD)Flibe/(nu)Flibe

        # uD Flibe = nuFlibe*ReTherminol

        # now this is not a standard or common function which i will use repeatedly so i will just type it explicitly

        uDFlibe = self.getTherminolReynolds()*self.getFlibeNu()

        # note that when i adjust the therminol temperature, ideally the flibe temperature should also auto-adjust

        print("uD (velocity times Characteristic Diameter) of flibe is:")
        print(str(uDFlibe)+" m^2/s")

        # now i want to auto adjust the temperature of flibe and therminol when prandtl similarity is switched on

        # that's a bit of a challenge since we don't have a formula or correlation we can use
        # and the scaling isn't exactly linear

        # one solution is to plot prandtl numbers of both CIET and flibe vs temperature
        # and just try to get a best fit

        # to get such a best fit, we will need to translate the temperature scales of ciet up to the prototypical system
        # this requires a scaling factor (usually 10/3) and a fixed point (the lower end is about 45C Dowtherm/Therminol,
        # and 520C Flibe)
        # that's according to figure 2 on the graph

        # to do this i created a set therminol temperature method, which does all this,
        # if prandtl simultude is true, then the thing is autosynced


        # if you want to see the prandtlNumberTest results, use:  self.prandtlNumberMatchTest()

        # so let me recalculate uD again given the above

        # and now it seems that this function may be used sometimes in the context of testing so i'll just design a function

        self.printBar()
        print("now we want to obtain uD after the set temperature method")
        self.setTherminolTemperature(80)

        uDFlibe = self.getUDFlibe()
        print("uD (velocity times Characteristic Diameter) of flibe is:")
        print(str(uDFlibe)+" m^2/s")

        print("flibe reynolds is equal to therminol reynolds:")

        print(self.getTherminolReynolds())

        print("flibe kinematic viscosity is (m^2/s):")
        print(self.getFlibeNu())

        # so now we have Pr similtude via temperature simuliltude
        # and Re simultude via flow similtude,
        # thus we should match Nusselt number (dimensionless heat transfer rates through to the wall)

        # heat capacity and all the rest of the things are NOT matched, and will have to be programmed into the heater

        # now since we have uD flibe, we just need a length ratio of the prototypical fhr and the model
        # this should be relatively straightforward (i hope)

        # now i only match forced convection phenomena, however the thermal inertia and everything else will have to be calculated after
        # the length scales are determined...


        # first order of business:

        # let's get the power ratios, i use M2P to denote model to prototype ratio,
        # ie ciet to reactor
        
        powerRatioM2P = self.getCietPowerWatts()/self.getReactorPowerWatts()

        print('power ratio of model to prototype full reactor')
        print(powerRatioM2P)

        volHeatCapacityRatioM2P = self.therminolPropertiesObj.getVolumetricHeatCapacity()/self.flibePropertiesObj.getVolumetricHeatCapacity()

        print('volumetric heat capacity ratio model to prototype full reactor')
        print(volHeatCapacityRatioM2P)

        # then temperature ratios

        temperatureDiffRatioM2P = self.cietObj.getDeltaT()/self.reactorObj.getDeltaT()

        print('temperature difference ratios model to prototype full reactor')
        print(temperatureDiffRatioM2P)

        # lastly UL ratios which is 1

        uLRatioM2P = 1

        lengthRatioM2P = powerRatioM2P/volHeatCapacityRatioM2P/temperatureDiffRatioM2P/uLRatioM2P

        print('length ratio model to prototype calculated as:')
        print(lengthRatioM2P)

        # ratio  = Lm/Lp, Lp = Lm/ratio

        prototypeDiameter = self.cietObj.getInnerDiameter()/lengthRatioM2P

        print('prototype lengthscale and diameter in meters:')
        print(prototypeDiameter)

        # then we calculate velocity

        prototypeVelocity = uDFlibe/prototypeDiameter

        print('prototype velocity m/s:')
        print(prototypeVelocity)

        # then area scale

        prototypeArea = prototypeDiameter**2*self.np.pi/4

        print('prototype area in m^2')
        print(prototypeArea)

        return 0
    
    def getUDFlibe(self):

        # returns the product of flibe velocity and characteristic pipe diameter in m^2/s

        if self.getFlowSimiltude() == True:
            reynoldsNumber = self.getTherminolReynolds()

        uDFlibe = reynoldsNumber*self.getFlibeNu()

        return uDFlibe




    def prandtlNumberMatchTest(self):

        self.setTherminolTemperature(80)
        self.printPrandtlNumberComparison()

        self.setTherminolTemperature(90)
        self.printPrandtlNumberComparison()

        self.setTherminolTemperature(100)
        self.printPrandtlNumberComparison()

        self.setTherminolTemperature(110)
        self.printPrandtlNumberComparison()


        self.setTherminolTemperature(45)
        self.printPrandtlNumberComparison()
        self.setTherminolTemperature(85)
        self.printPrandtlNumberComparison()

        self.setFlibeTemperature(650)
        self.printPrandtlNumberComparison()

        return 0





##### the following section has get and set functions #####

    def setFlowSimiltude(self,flowSimiltude=True):

        # flowSimiltude variable is here, setting it as true ensures that the
        # Re of the scaled (therminol) model is the same as 
        # Re of the prototypical (flibe) model

        self.flowSimiltude=flowSimiltude

        # this uses a True or False Boolean

        if flowSimiltude == 'true':
            self.flowSimiltude = True
        elif flowSimiltude == 'false':
            self.flowSimiltude = False


    def setPrandtlSimiltude(self,prandtlSimiltude=True):

        # flowSimiltude variable is here, setting it as true ensures that the
        # Re of the scaled (therminol) model is the same as 
        # Re of the prototypical (flibe) model

        self.prandtlSimiltude=prandtlSimiltude

        # this uses a True or False Boolean

        if prandtlSimiltude == 'true':
            self.prandtlSimiltude = True
        elif prandtlSimiltude == 'false':
            self.prandtlSimiltude = False

    def getFlowSimiltude(self):

        return self.flowSimiltude

    def getPrandtlSimiltude(self):

        return self.prandtlSimiltude

    def getTherminolPropertiesObj(self):

        # this helps to return the therminol properties object
        from therminolFlibe import therminolProperties

        therminolPropertiesObj = therminolProperties()

        return therminolPropertiesObj


    def getFlibePropertiesObj(self):

        # this helps to return the therminol properties object
        from therminolFlibe import flibeProperties

        flibePropertiesObj = flibeProperties()

        return flibePropertiesObj

    def getCietObj(self):

        # this helps to return the ciet object which contains methods and data
        # required for calculation of ciet stuff

        from therminolFlibe import cietHeater

        cietObj = cietHeater()

        return cietObj


    def getReactorObj(self):

        # this helps to return the prototypical object which contains methods and data
        # required for calculation of ciet stuff

        from therminolFlibe import prototypicalReactor

        reactorObj = prototypicalReactor()

        return reactorObj



    def setTherminolVelocity(self,uTherminol=0):

        # sets velocity in m/s

        self.uTherminol = uTherminol

    def setFlibeVelocity(self,uFlibe=0):

        # sets velocity in m/s

        self.uFlibe = uFlibe

    def getTherminolVelocity(self):

        return self.uTherminol

    def getFlibeVelocity(self):

        return self.uFlibe

    def setTherminolDiameter(self,dTherminol=0):

        # sets therminol model diameter in m

        self.dTherminol = dTherminol

    def getTherminolDiameter(self):

        return self.dTherminol

    def setFlibeDiameter(self,dFlibe=0):

        # sets flibe prototypical diameter in m

        self.dFlibe = dFlibe

    def getFlibeDiameter(self):

        return self.dFlibe


    def getTherminolNu(self):

        return self.therminolPropertiesObj.getNu()

    def getFlibeNu(self):

        return self.flibePropertiesObj.getNu()



    def getTherminolReynolds(self):

        return self.getTherminolVelocity()*self.getTherminolDiameter()/self.getTherminolNu()

    def getFlibeReynolds(self):

        return self.getFlibeVelocity()*self.getFlibeDiameter()/self.getFlibeNu()


    def getTherminolPrandtl(self):

        return self.therminolPropertiesObj.getPr()

    def getFlibePrandtl(self):

        return self.flibePropertiesObj.getPr()

    def setCietMassFlowrate(self,massFlowrate):

        # loads ciet's mass flowrate in kg/s

        self.cietMassFlowrate = massFlowrate

    def getCietMassFlowrate(self):

        return self.cietMassFlowrate

    def setCietPowerWatts(self,cietPowerWatts):

        self.cietPowerWatts = cietPowerWatts

    def getCietPowerWatts(self):

        return self.cietPowerWatts

    def setReactorPowerWatts(self,reactorPowerWatts):

        self.reactorObj.setPowerWatts(reactorPowerWatts)

    def getReactorPowerWatts(self):

        return self.reactorObj.getPowerWatts()

    def setReactorInnerDiameter(self,reactorInnerDiameter):

        # sets reactor inner diameter in meters

        self.reactorInnerDiameter = reactorInnerDiameter

    def getReactorInnerDiameter(self):

        return self.reactorInnerDiameter

##### here are functions to sync things with the reactor object #####

    def syncReactorObj(self):

        # note, the reactor does not need its flowrate set since it is calculated

        # next i'm going to get flibe density and pass it on to the reactor obj

        flibeDensity = self.flibePropertiesObj.getRho()
        self.reactorObj.setFluidDensity(flibeDensity)

        # next i'm going to set the inner diameter (this has to be calculated via power scaling)

        # to be continued...







##### here are functions to synchronise things with ciet object #####


    def syncCietObj(self):

        # this function is here to get the cietObj to load in the velocity and diameter
        # for calculation of therminol Re given a mass flowrate 

        self.cietObj.setMassFlowrate(self.getCietMassFlowrate())
        self.cietObj.setPowerWatts(self.getCietPowerWatts())


        # next i'm going to get the density of therminol and pass it on to the ciet obj        

        therminolDensity = self.therminolPropertiesObj.getRho()
        self.cietObj.setFluidDensity(therminolDensity)

        # then i'm going to use the ciet object inner diameter and sync that with
        # the existing object

        therminolDiameter = self.cietObj.getInnerDiameter()
        self.setTherminolDiameter(therminolDiameter)

        # last but not least i'll sync the therminol velocity obtained from mass flowrate
        # into the scaling object (this class)

        therminolVelocity = self.cietObj.getFluidVelocity() 
        self.setTherminolVelocity(therminolVelocity)


    def syncCietTemperatureToReactor(self):

        # now i want to auto adjust the temperature of flibe and therminol when prandtl similarity is switched on

        # that's a bit of a challenge since we don't have a formula or correlation we can use
        # and the scaling isn't exactly linear

        # one solution is to plot prandtl numbers of both CIET and flibe vs temperature
        # and just try to get a best fit

        # to get such a best fit, we will need to translate the temperature scales of ciet up to the prototypical system
        # this requires a scaling factor (usually 10/3) and a fixed point (the lower end is about 45C Dowtherm/Therminol,
        # and 520C Flibe)
        # that's according to figure 2 on the graph

        # https://www.researchgate.net/publication/311212126_Design_of_the_Compact_Integral_Effects_Test_Facility_and_Validation_of_Best-Estimate_Models_for_Fluoride_Salt-Cooled_High-Temperature_Reactors

        self.printBar()
        print('syncing ciet temperature to flibe temperature')

        cietTemperature = self.therminolPropertiesObj.getTemperature()

        cietTemperatureDifference = cietTemperature-self.getTherminolReferenceTemperature()

        flibeTemperatureDifference = cietTemperatureDifference*self.reactorObj.getDeltaT()/self.cietObj.getDeltaT()

        flibeTemperature = self.getFlibeReferenceTemperature()+flibeTemperatureDifference

        self.flibePropertiesObj.setTemperature(flibeTemperature)


    def syncReactorTemperatureToCiet(self):

        self.printBar()
        print('syncing reactor(flibe) temperature to ciet(therminol) temperature')

        flibeTemperature = self.getFlibeTemperature()

        flibeTemperatureDifference = flibeTemperature - self.getFlibeReferenceTemperature()
        
        cietTemperatureDifference = flibeTemperatureDifference * self.cietObj.getDeltaT()/self.reactorObj.getDeltaT()

        cietTemperature = self.getTherminolReferenceTemperature()+cietTemperatureDifference

        self.therminolPropertiesObj.setTemperature(cietTemperature)

    def getTherminolReferenceTemperature(self):

        # returns the therminol reference temperature in degrees C

        # this is used to sync therminol temperatures with flibe temperatures
        # and vice versa

        return 80

    def getFlibeReferenceTemperature(self):

        # returns the flibe reference temperature in degrees C

        # this is used to sync therminol temperatures with flibe temperatures
        # and vice versa

        # this was manually tested to match the prandtl numbers as close as possible in the 80C to 110C range

        return 600

    
    def setTherminolTemperature(self,T):

        # sets therminol temperature in degrees C

        self.therminolPropertiesObj.setTemperature(T)

        if self.getPrandtlSimiltude() == True:
            self.syncCietTemperatureToReactor()
            self.syncCietObj()


    def getTherminolTemperature(self):

        return self.therminolPropertiesObj.getTemperature()

    def setFlibeTemperature(self,T):

        # sets flibe temperature in degrees C

        self.flibePropertiesObj.setTemperature(T)

        if self.getPrandtlSimiltude() == True:
            self.syncReactorTemperatureToCiet()
            self.syncCietObj()



    def getFlibeTemperature(self):

        return self.flibePropertiesObj.getTemperature()






##### here are printing functions #####


    def printStatus(self):

        self.printIntroBar()
        print('flow similtude is set to:')
        print(self.getFlowSimiltude())

        print('prandtl similtude is set to:')
        print(self.getPrandtlSimiltude())

        self.printOutroBar()


    def printOutroBar(self):

        print(' ')
        print(' ')
        print('==============================================')
        print(' ')
        print(' ')


    def printIntroBar(self):

        print(' ')
        print(' ')
        print('==============================================')
        print(' ')
        print(' ')


    def printBar(self):

        self.printIntroBar()


    def printPrandtlNumberComparison(self):


        print('therminol temperature is:')
        print(self.getTherminolTemperature())
        print('therminol Prandtl Number: '+str(self.getTherminolPrandtl()))
        print('flibe temperature is:')
        print('flibe Prandtl Number: '+str(self.getFlibePrandtl()))
        print(self.getFlibeTemperature())

########################################################################################
########################################################################################
########################################################################################
########################################################################################

# this section pertains to classes talking about the primary loop systems
# for example, CIET's heater and the reactor

class primaryLoops:

    def importNumpy(self):

        import numpy as np
        self.np = np


    def printHelp(self):

        print('this is a class which contains data and methods pertaining to primary loops')

        print(r"for example CIET's heater and the actual reactor")


    def setDeltaT(self,T):

        # sets delta T in degrees or kelvin
        self.deltaT = T

    def getDeltaT(self):

        return self.deltaT

    def setMassFlowrate(self,massFlowrate):

        # sets mass flowrate in kg/s

        self.massFlowrate = massFlowrate

    def getMassFlowrate(self):

        return self.massFlowrate


    def setPowerWatts(self,powerWatts):

        # sets the power in watts

        self.powerWatts = powerWatts

    def getPowerWatts(self):

        return self.powerWatts

    def setFluidDensity(self,fluidDensity):

        # sets fluid density in kg/m3

        self.fluidDensity = fluidDensity

    def getFluidDensity(self):

        return self.fluidDensity()

    def getInnerDiameter(self):

        print(r"note, this is only a placeholder function")
        print(r"it will just return a diameter of 0")
        print(r"to replace it, define getInnerDiameter in the child classes")

        return 0

    def getCrossSectionalArea(self):

        # returns cross sectional area in m2 provided
        # that the cross section is circular in shape

        self.importNumpy()

        return self.getInnerDiameter()**2/4*self.np.pi


    def setFluidDensity(self,density='default'):


        if density == 'default':

            print('density set to default of 1000 kg/m3')
            print('please use the setFluidDensity method to set the correct density')

            density = 1000

        # sets Fluid Density in kg/m3

        self.fluidDensity = density

    def getFluidDensity(self):

        return self.fluidDensity

    def getFluidVelocity(self):

        # returns fluid density if mass flowrate, fluid density and cross sectional area are given

        fluidVelocity = self.getMassFlowrate()/self.getFluidDensity()/self.getCrossSectionalArea()

        return fluidVelocity

    





class cietHeater(primaryLoops):


    def __init__(self):



        self.importNumpy()

        self.setFluidDensity()

        self.setCIETConstraintsAndParameters()

    def setCIETConstraintsAndParameters(self):

        # now given that CIET's flowrates and power ratings constrain the reactor, we'll need to set those down

        # CIET's mass flowrate is in kg/s

        self.setMassFlowrate(0.18)
        self.setPowerWatts(10000)
        self.setDeltaT(30)

        # for ciet pipe inner diameter, see:

        # https://www.researchgate.net/publication/311212126_Design_of_the_Compact_Integral_Effects_Test_Facility_and_Validation_of_Best-Estimate_Models_for_Fluoride_Salt-Cooled_High-Temperature_Reactors/link/58eaede5aca2729d8cd59c6b/download

    def printDiameterDetails(self):

        print(r"see publication in:")

        print(r"https://www.researchgate.net/publication/311212126_Design_of_the_Compact_Integral_Effects_Test_Facility_and_Validation_of_Best-Estimate_Models_for_Fluoride_Salt-Cooled_High-Temperature_Reactors")

        print(' ')

        print(r"the diameter quote here is this:")

        print(r"The experimental loop uses Schedule 10 Type 304L stainless steel ")
        print(r"piping of 0.28-cm wall thickness and 3.34-cm outside diameter (o.d.)")
        print(r"(1.315-in. o.d., 1 in. nominal).")


    def getCietInnerDiameterCentimeters(self):

        # returns ciet's pipe diameter in centimeters

        return self.getCietOuterDiameterCentimeters()-self.getCietWallThicknessCentimeters()*2

    def getCietWallThicknessCentimeters(self):

        return 0.28

    def getCietOuterDiameterCentimeters(self):

        return 3.34

    def getInnerDiameter(self):

        # returns ciet's inner diameter in meters

        return self.getCietInnerDiameterCentimeters()/100







class prototypicalReactor(primaryLoops):



    def __init__(self):

        self.importNumpy()
        self.setReactorConstraintsAndParameters()

    def setReactorConstraintsAndParameters(self):

        # first i have a 45MWt reactor (of course i can change this)
        self.setPowerWatts(4.5e6)
        self.setDeltaT(100)



########################################################################################
########################################################################################
########################################################################################
########################################################################################


# here is a parent class that can store some important attributes and methods for therminol
# and flibe

class thermophysicalPropertiesCalc:

    def importNumpy(self):

        import numpy as np
        self.np = np

    def setTemperatureKelvin(self,T):

        # sets temperature using kelvin

        self.temperature = T - 273.15

    def setTemperatureCelsius(self,T):

        self.temperature = T

    def getTemperatureCelsius(self):

        return self.temperature

    def getTemperatureKelvin(self):

        return self.temperature+273.15

    def getTemperature(self,mode='celsius'):

        if mode == 'celsius':
            return self.getTemperatureCelsius()

        elif mode == 'kelvin':
            return self.getTemperatureKelvin()

    def getNu(self):

        # calculates kinematic viscosity in SI units

        return self.getMu()/self.getRho()

    def getVolumetricHeatCapacity(self):

        # calculates volumetric heat capacity in SI units

        return self.getRho()*self.getCP()

    def getPr(self):

        # calculates the prandtl number at said temperature

        return self.getMu()*self.getCP()/self.getK()


    def getAlpha(self):

        # calculates the thermal diffusivity of the fluid at said temperature

        return self.getNu()/self.getPr()

    def setTemperature(self,T,mode='celsius'):

        if mode == 'celsius':
            self.setTemperatureCelsius(T)
            self.printBar()
            print('setting ' + self.getMaterialName() +' temperature to '+str(T)+' celsius')

        elif mode == 'kelvin':
            self.setTemperatureKelvin(T)
            self.printBar()
            print('setting ' + self.getMaterialName() + '  temperature to '+str(T)+' kelvin')

        else:
            print('only celsius and kelvin are valid modes to use for setTemperature methods')
            print('please use the correct mode')

# we have get and set functions for material name

    def setMaterialName(self,materialName):

        self.materialName = materialName

    def getMaterialName(self):

        return self.materialName

# also a printBar function to make the printing part look nice

    def printBar(self):

        print(' ')
        print(' ')
        print('==============================================')
        print(' ')
        print(' ')


# here are classes to store thermophysical properties of the fluids, eg dowtherm/therminol

# these will inherit functions from the parent class, properties

class therminolProperties(thermophysicalPropertiesCalc):

    def __init__(self):

        self.setMaterialName('therminol/DowthermA')

        print(' ')
        print(self.getMaterialName()+' properties object initiated!')
        print(' ')

        self.printBar()
        self.printUnits()
        self.printBar()
        self.setTemperature(80)
        self.printBar()
        
    def printUnits(self):
        print('units used are:')
        print('temperature: degrees C')
        print('dyanmic viscosity Pa second')
        print('specific heat capacity J/(kg K)')
        print('fluid thermal conductivity W/(m K), watts per meter per kelvin or degC')
        print('fluid density kg/m3')

    def printFormulas(self):

        print('formulas used are (for '+ self.getMaterialName() + ' ):')

        print('dynamic viscosity:')
        print('mu = 0.130/(T^1.072)')

        print(' ')
        print('specific heat capacity:')
        print('cp = 1518 + 2.82*T')

        print(' ')
        print(self.getMaterialName() + ' fluid thermal conductivity:')
        print('k = 0.142 - 0.00016*T')
        
        print(' ')
        print(self.getMaterialName()+ ' fluid density:')
        print('rho = 1078 -0.85*T')

    def printValidRange(self):

        self.printBar()
        print('these formulas are only for Dowtherm A on 20C to 180C')
        print('C as in celsius')
        print('but can be used for therminol also since composition is very similar')
        print('like to within 0.5% composition of the two constituent oils:')
        print('diphenyl oxide (DPO) and biphenyl')

        self.printBar()

    def printCitationWebsite(self):

        print(r'https://www.tandfonline.com/doi/abs/10.13182/NT16-15')


    def getMu(self):

        # calculates dynamic viscosity

        T = self.getTemperature()

        return 0.130/(T**1.072)

    def getCP(self):

        # calculates specific heat capacity

        return 1518 + 2.82*self.getTemperature()

    def getK(self):

        # calculates thermal conductivity

        return 0.142-0.00016*self.getTemperature()

    def getRho(self):

        # calculates density

        return 1078-0.85*self.getTemperature()



# https://dspace.mit.edu/bitstream/handle/1721.1/123988/Romatoski_SaltPropertyReview02.pdf?sequence=1&isAllowed=y

class flibeProperties(thermophysicalPropertiesCalc):

    def __init__(self):

        self.setMaterialName('FLiBe 66(LiF)-34(BeF2) mol% eutectic')
        self.importNumpy()

        print(' ')
        print(self.getMaterialName()+' properties object initiated!')
        print(' ')

        self.printBar()
        self.printUnits()
        self.printBar()
        self.setTemperature(600)
        self.printBar()
        
    def printUnits(self):
        print('units used are:')
        print('temperature: celsius')
        print('dyanmic viscosity Pa second')
        print('specific heat capacity J/(kg K)')
        print('fluid thermal conductivity W/(m K), watts per meter per kelvin or degC')
        print('fluid density kg/m3')

        print('correlations use different units, but they will be converted to SI before calculation')

    def printFormulas(self):

        print('formulas used are (for '+ self.getMaterialName() + ' ):')

        print(r'dynamic viscosity:')
        print(r'mu = 0.07803*exp(4022/T(K)) centipoise')
        print(r'note, valid range in temp is 812.5 kelvin to 1573 kelvin')
        print(r'uncertaintiy +/- 1.3%')

        print(' ')
        print('specific heat capacity:')
        print('cp = 2386 J/(kg K)')

        print(' ')
        print(self.getMaterialName() + 'fluid thermal conductivity:')
        print('k = 1.1 W/(m K)')
        print('uncertainty +/- 10%')
        
        print(' ')
        print(self.getMaterialName() + ' fluid density:')
        print('rho = 2415.6 – 0.49072*T[K]')
        print('valid range 732.2 - 4498.8K')

    def printValidRange(self):

        self.printBar()
        print('these formulas are only for ' +self.getMaterialName()  )
        print('density correlations range from 732.2-4498.8K')
        print('heat capacity correlations range from 600-1200K')

        print('thermal conductivity estimates range from 750K to 1200K')
        print('last but not least, dynamic viscosity correlations range from 812.5-1573K')

        print('from these correlations, the narrowest range for calculating Re, Pr is 812.5K to 1200K')

        self.printBar()

    def printCitationWebsite(self):

        print(r'https://dspace.mit.edu/bitstream/handle/1721.1/123988/Romatoski_SaltPropertyReview02.pdf?sequence=1&isAllowed=y')


    def getMuCentipoise(self):

        # calculates dynamic viscosity using temperature in kelvin

        T = self.getTemperature(mode = 'kelvin')

        # note, the correlation used here is for LiF 67.2% and BeF2 32.8% [mol%]
        # not exactly 66% 34% but we hope the difference is trivial enough to be ignored for purposes of similtude

        np = self.np
        coefficient = 0.07803
        exponent = np.exp(4022/T)

        return coefficient*exponent

    def getMu(self):

        # calculates dynamic viscosity in Pa s or SI units

        muCentipoise = self.getMuCentipoise()

        mu = 0.001*muCentipoise

        return mu

    def getCP(self):

        # calculates specific heat capacity

        return 2386

    def getK(self):

        # calculates thermal conductivity

        return 1.1

    def getRho(self):

        # calculates density

        return 2415.6-0.49072*self.getTemperature(mode = 'kelvin')

########################################################################################
########################################################################################
########################################################################################
########################################################################################

class test:

    def __init__(self):

        self.buildScalingObj()

    def buildScalingObj(self):

        from therminolFlibe import scalingClass

        self.scalingObj = scalingClass()

        return self.scalingObj

    def getScalingObj(self):

        return self.scalingObj

    def test1(self):

        # this test initialises the scaling class

        self.scalingObj.printStatus()

        scalingObj = self.getScalingObj()

        therminolProps = scalingObj.getTherminolPropertiesObj()

        alphaTherminol = therminolProps.getAlpha()

        print('thermal diffusivity of '+ therminolProps.getMaterialName()  +  '  test:')
        print(alphaTherminol)

        print(' ')
        print('now for flibe')

        flibeProps = scalingObj.getFlibePropertiesObj()

        alphaFlibe = flibeProps.getAlpha()

        print('thermal diffusivity of '+ flibeProps.getMaterialName() + '  test:')
        print(alphaFlibe)

        therminolReynolds = scalingObj.getTherminolReynolds()

        print(r"reynold's number of therminol")

        print(therminolReynolds)

        print(r"reynold's nuber of flibe")

        print(scalingObj.getFlibeReynolds())

        print(r"get prandtl number of therminol and Flibe")

        print(scalingObj.getTherminolPrandtl())
        print(scalingObj.getFlibePrandtl())


    def test2(self):

        # this test will test the ciet and reactor object functions and methods

        self.scalingObj.printStatus()

        scalingObj = self.getScalingObj()

        cietObj = scalingObj.getCietObj()
        reactorObj = scalingObj.getReactorObj()

        # now let's set the temperature of therminol

        scalingObj.therminolPropertiesObj.setTemperature(90)

        # we'll get the density of therminol at this temperature

        therminolDensity = scalingObj.therminolPropertiesObj.getRho()

        cietObj.setFluidDensity(therminolDensity)

        print('therminol density at 90C:')
        print(cietObj.getFluidDensity())

        # based on that, a mass flowrate of 0.18 kg/s, inner diameter as stipulated in the paper, we will get
        # a fluid velocity



        print('therminol velocity at 90C at ' + str(cietObj.getMassFlowrate())  +  ' kg/s flowrate')

        print(str(cietObj.getFluidVelocity()) + ' m/s')


        # now we will try to integrate this object into the overall flow

        # i'm going to use the cietObj to help set the therminol velocity manually
        # i can use functions to do this in future

        scalingObj.setTherminolVelocity(cietObj.getFluidVelocity())
        scalingObj.setTherminolDiameter(cietObj.getInnerDiameter())
        print("reynolds test calc")

        print(' ')
        print(str(scalingObj.getTherminolReynolds()))

    def test3(self):

        # now assume that therminol and ciet models are okay,
        # we now want to scale up to flibe

        # to do so, we'll need a function to scale things up

        # first by setting flibe with a reynold's number
        # and matching Pr manually using some correlation since there is no 100\% match between
        # Pr of therminol and Pr of flibe at the said temperatures

        # so first, we'll get two functions, one to match Prandtl number
        # one to match reynold's number, or set reynold's number of Flibe to be equal to that of flibe

        # once that is done, we should be able to extract a characteristic velocity*Distance from Re
        # the third function's job is to return this value of velocity*Distance


        # given that, and looking back at the power correlation, 

        # we should be able to solve for the lengthscale of the prototypical system
        # and from the velocity*Distance product, obtain the velocity
        # and therefore mass flowrate

        # then we should also be able to get the area scaling as well from these calculations

        # now this is a crude way of scaling up, so it will be iteration 1 scaleup
        # that will be the driver function for all these other functions
        # this will all be done in test3


        self.scalingObj.printStatus()
        scalingObj = self.getScalingObj()

        scalingObj.iterationOneScaleUp()




class workspace:

    def __init__(self):

        print('initialising workspace')

        self.initialiseDefaults()

    def initialiseDefaults(self):

        import therminolFlibe
        from therminolFlibe import scalingClass
        from therminolFlibe import test


        self.therminolFlibe = therminolFlibe
        self.scalingClass = scalingClass
        self.test = test

        from importlib import reload
        self.reload = reload

    def reloadClasses(self):

        reload = self.reload
        import therminolFlibe
        reload(therminolFlibe)

        # once therminolFlibe is reloaded, we then set all our other classes to eb the latest version

        from therminolFlibe import scalingClass
        from therminolFlibe import test


        self.therminolFlibe = therminolFlibe
        self.scalingClass = scalingClass
        self.test = test


    def getTestObj(self):

        self.reloadClasses()
        self.testObj = self.test()

        return self.testObj

    def getScalingObj(self):

        self.reloadClasses()
        self.scalingObj = self.scalingClass()

        return self.scalingObj


def printHelp():

    print(' ')
    print('welcome to the scaling class for therminol and flibe')
    print('to load workspace objects use:')
    
    print(' ')
    print('from therminolFlibe import workspace')
    print('self = workspace()')

    print(' ')
    print('Then to load tests and scaling objects respectively use:')

    print('test = self.getTestObj()')
    print('scalingObj = self.getScalingObj()')
    print(' ')

printHelp()
